transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN {C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN/PRBS_PAMN.v}
vlog -vlog01compat -work work +incdir+C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN {C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN/PRBS.v}

vlog -vlog01compat -work work +incdir+C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN {C:/Users/silvi/Documents/Quartus_proyects/PRBS_PAMN/tb_PrbsGen.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L stratixiv_hssi_ver -L stratixiv_pcie_hip_ver -L stratixiv_ver -L rtl_work -L work -voptargs="+acc"  tb_PrbsGen

add wave *
view structure
view signals
run -all
