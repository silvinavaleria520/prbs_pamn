module PRBS
#(
    parameter                                           NB_DATA   = 8,
    parameter                                           NB_SEED   = 9,
    parameter                                           N_PRBS_SEED    = 9'h1AA  // 110101010                                  
)
(
    // OUTPUTS.                                 
    output  wire    [NB_DATA-1:0]                       prbs_9_out,                                                
    // Synchronous reset signal    
    input   wire                                        i_reset,
    // System clock.
    input   wire                                        i_clock

);


// INTERNAL SIGNALS
    reg           [NB_SEED          -1:0]               state     ;
    reg           [NB_SEED          -1:0]               next_state;
    wire          [NB_SEED          -1:0]               next_state_9prbs;
	



always @ ( posedge i_clock or posedge i_reset )
    begin:reg_seed
        if (i_reset)
            state <= N_PRBS_SEED;
        else 
            state <= next_state;
    end 

always @(*) 
begin

  next_state = next_state_9prbs;
    
end


/*	//PRBS9 Polynomial - 1 + X^5 + X^9
	assign  prbs_9_out[  7] =  state[ 8]; // 1
	assign  prbs_9_out[  6] =  state[ 7]; // 1 
	assign  prbs_9_out[  5] =  state[ 6]; // 0
	assign  prbs_9_out[  4] =  state[ 5]; // 1
	assign  prbs_9_out[  3] =  state[ 4]; // 0
	assign  prbs_9_out[  2] =  state[ 3]; // 1
	assign  prbs_9_out[  1] =  state[ 2]; // 0
	assign  prbs_9_out[  0] =  state[ 1]; // 1

*/

    assign  prbs_9_out[  7] =  state[ 1]; 
	assign  prbs_9_out[  6] =  state[ 2];  
	assign  prbs_9_out[  5] =  state[ 3]; 
	assign  prbs_9_out[  4] =  state[ 4]; 
	assign  prbs_9_out[  3] =  state[ 5]; 
	assign  prbs_9_out[  2] =  state[ 6]; 
	assign  prbs_9_out[  1] =  state[ 7]; 
	assign  prbs_9_out[  0] =  state[ 8]; 

	assign  next_state_9prbs[ 0] =  state[ 1] ^ state[ 2] ^ state[ 6];   // 1
	assign  next_state_9prbs[ 1] =  state[ 2] ^ state[ 3] ^ state[ 7];   // 0
	assign  next_state_9prbs[ 2] =  state[ 3] ^ state[ 4] ^ state[ 8];   // 0
	assign  next_state_9prbs[ 3] =  state[ 0] ^ state[ 4];               // 0
	assign  next_state_9prbs[ 4] =  state[ 1] ^ state[ 5];               // 0
	assign  next_state_9prbs[ 5] =  state[ 2] ^ state[ 6];               // 0
	assign  next_state_9prbs[ 6] =  state[ 3] ^ state[ 7];               // 0
	assign  next_state_9prbs[ 7] =  state[ 4] ^ state[ 8];               // 1
	assign  next_state_9prbs[ 8] =  state[ 0];                           // 0


    

	


endmodule