module PRBS_PAMN
#( 
	parameter N_BITS = 8,
	parameter NB_DATA =16

 )
(
	input  clock,
	input  reset,
	input  [1:0] enable ,        /*El nios envia este dato indica la cuantas prbs se va a instanciar para poder variar de PAM2-PAM32*/
   // input [1:0] prbs_mode,    /*El nios envia este dato indica el largo de la prbs a instanciar .Ej PRBS9-->PRBS31               */
	output [N_BITS*NB_DATA-1:0] prbsData
);

 parameter NB_DATA_PRBS = 8;
 parameter NB_SEED   = 9;
 parameter PRBS_BITS = 3 ;
 parameter [(5*9)-1:0] SEEDS_9 = {9'h1AA,9'h1AA,9'h1A1,9'h1FE,9'h1AA};
 parameter [(5*9)-1:0] SEEDS_31 = {9'h1AA,9'h1AA,9'h1A1,9'h1FE,9'h1AA};
 genvar i;
 wire [7:0] o_data [4:0];
 wire [4:0] o_bits_prbs [7:0];



generate

  for(i=0;i<PRBS_BITS;i=i+1)
  begin:inst
  //if(prbs_mode==XX)
     PRBS
         #(
            .NB_DATA(NB_DATA_PRBS),
            .NB_SEED (NB_SEED),
            .N_PRBS_SEED (SEEDS_9[(i*9)+:9])      
          )
         u_PRBS(
            .i_clock(clock),
            .i_reset(reset),
            .prbs_9_out(o_data[i])
            // i_prbs_mode(prbs_mode)
          );
     
  end
endgenerate



/*
generate
       
       for(i=0;i<N_BITS;i=i+1)
       begin
          o_bits_prbs={o_data[0][i], o_data[1][i], o_data[2][i] ,o_data[3][i] ,o_data[4][i]};
       end


endgenerate*/

/*
        o_data[0][0] o_data[1][0] o_data[2][0] o_data[3][0] o_data[4][0]  
        o_data[0][1] o_data[1][1] o_data[2][1] o_data[3][1] o_data[4][1]
        ...............................................................
        o_data[0][7] o_data[1][7] o_data[2][7] o_data[3][7] o_data[4][7]*/













endmodule