`timescale 1ns/100ps
module tb_PrbsGen();
  
  
  
  parameter N_BITS = 8;
  parameter NB_DATA =16;
  reg clock;
  reg reset;
  reg [1:0]enable;
  
  wire [128-1:0] prbsData;
  
  
 
 
 	
PRBS_PAMN
#(
  .N_BITS(N_BITS),
  .NB_DATA(NB_DATA)
  )
	u_PRBS_PAMN
	(
	  .clock(clock),
	  .reset(reset),
	  .enable(enable),
	  .prbsData(prbsData)
	  
	  
	);
  
  

  
  always #5 clock = ~clock;

  
  initial begin
  
     clock = 1'b0;       
	  reset   = 1'b1;
	  enable  = 2'b00;
    #5 reset   = 1'b0;
	  #1000 enable = 2'b01;
    #10000 reset = 1'b1;
	  #20000 $finish   ;
      


  end






endmodule